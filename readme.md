Packet Parser
=============

Just trying to sort out a kind of structure for doing this.

Build
-----

Uses cmake to build.

```sh
$ mkdir build
$ cd build
$ cmake ..
$ make
```

After building the tests can be run with:

```sh
$ make test
```
