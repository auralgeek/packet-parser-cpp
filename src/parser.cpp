#include <cstdint>
#include <memory>
#include <iterator>
#include <variant>
#include <vector>

#include "parser.hpp"

PacketParser::PacketParser(const std::vector<uint32_t> &data) {
    m_data = std::make_shared<const std::vector<uint32_t>>(data);
    m_idx = 0;
};

PacketType1 PacketParser::parse_pkt_1()
{
    PacketType1 output;
    uint32_t datalen = (*m_data)[m_idx++];

    for (size_t i = 0; i < datalen; i++) {
        output.data.push_back((*m_data)[m_idx++]);
    }

    return output;
}

PacketType2 PacketParser::parse_pkt_2()
{
    PacketType2 output;

    output.metadata1 = (*m_data)[m_idx++];
    output.metadata2 = (*m_data)[m_idx++];

    return output;
}

std::variant<PacketType1, PacketType2> PacketParser::next() {
    std::variant<PacketType1, PacketType2> output;
    uint32_t packet_type = get_packet_type();

    switch ((PktType)packet_type) {
        case PktType::Type1:
            output = parse_pkt_1();
            break;
        case PktType::Type2:
            output = parse_pkt_2();
            break;
        default:
            break;
    }

    return output;
}

uint32_t PacketParser::get_packet_type() {
    uint32_t word = (*m_data)[m_idx++];
    return word;
}
