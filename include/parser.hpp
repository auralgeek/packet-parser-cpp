#pragma once

#include <cstdint>
#include <iterator>
#include <variant>
#include <vector>

enum class PktType : uint8_t {
    Type1,
    Type2,
};

/* Packet Type 1
 * [ ID | size | data ]
 */
struct PacketType1 {
    std::vector<uint32_t> data;
};

/* Packet Type 2
 * [ ID | metadata1 | metadata2 ]
 */
struct PacketType2 {
    uint32_t metadata1;
    uint32_t metadata2;
};

class PacketParser {
public:
    PacketParser(const std::vector<uint32_t> &data);
    std::variant<PacketType1, PacketType2> next();

private:
    uint32_t get_packet_type();
    PacketType1 parse_pkt_1();
    PacketType2 parse_pkt_2();
    std::iterator<std::forward_iterator_tag, uint32_t> m_input_it;

    std::shared_ptr<const std::vector<uint32_t>> m_data;
    size_t m_idx;
};
