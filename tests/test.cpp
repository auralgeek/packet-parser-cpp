#include <gtest/gtest.h>
#include <variant>

#include "parser.hpp"


TEST(ParserTest, Basic) {
    std::vector<uint32_t> data = {
        0x00000000, // ID = 0
        0x00000007, // size = 7 words
        0x00000001, // word 1
        0x00000002, // word 2
        0x00000003, // word 3
        0x00000004, // word 4
        0x00000005, // word 5
        0x00000006, // word 6
        0x00000007, // word 7
        0x00000001, // ID = 1
        0xdeadbeef, // metadata1
        0xcafebabe, // metadata2
        0x00000000, // ID = 0
        0x00000002, // size = 2 words
        0x00000001, // word 1
        0x00000002, // word 2
        0x00000000, // ID = 0
        0x00000001, // size = 1 word
        0x00000001, // word 1
    };

    PacketParser parser(data);

    PacketType1 packet1 = std::get<PacketType1>(parser.next());
    for (size_t i = 0; i < packet1.data.size(); i++) {
        EXPECT_EQ(packet1.data[i], i + 1);
    }

    PacketType2 packet2 = std::get<PacketType2>(parser.next());
    EXPECT_EQ(packet2.metadata1, 0xdeadbeef);
    EXPECT_EQ(packet2.metadata2, 0xcafebabe);

    packet1 = std::get<PacketType1>(parser.next());
    for (size_t i = 0; i < packet1.data.size(); i++) {
        EXPECT_EQ(packet1.data[i], i + 1);
    }

    packet1 = std::get<PacketType1>(parser.next());
    for (size_t i = 0; i < packet1.data.size(); i++) {
        EXPECT_EQ(packet1.data[i], i + 1);
    }
}
